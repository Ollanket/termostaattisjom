#include <avr/wdt.h> // Watchdog timer
#include <EEPROM.h>
#include <SPI.h>
#include <Ethernet.h>
#include <PubSubClient.h>
#include <LiquidCrystal_I2C.h>
#include <DallasTemperature.h>
#include <OneWire.h>
#include <Keypad.h>

// -----------------------ISR stuff------------------
#define HEATER_PIN 9
float deltaTemperature;     // Cant pass arguments to ISR so temperature data needs to be in a global variable.
                            // deltaTemperature = goal - measured temperature, update in the main loop.
bool heaterStatus = false;
//-----------------------end of ISR stuff------------
//-----OneWire stuff-----------

#define ONE_WIRE_BUS 8 
OneWire oneWire(ONE_WIRE_BUS);
DallasTemperature sensors(&oneWire);
//-----end of OneWire stuff----
// ----CONFIG--------
#define TARGET_TEMP_MIN 15
#define TARGET_TEMP_MAX 30
float targetTemperature;
// ----END CONFIG----
// ---ETHERNET-----
byte mac[] = {
  0x00, 0xAA, 0xBB, 0xCC, 0xDE, 0x02
};
EthernetClient ethClient;
PubSubClient client(ethClient);
const char* currentTempTopicName = "thermostat/currentTemp";
const char* targetTempTopicName = "thermostat/targetTemp";
const char* heaterOnTopicName = "thermostat/heaterOn";
const char* overrideOnTopicName = "thermostat/overrideOn";
const char* mqttServer = "termostaatti.duckdns.org";
const int mqttPort = 12779;
char dataString[50];
// --END ETHERNET--
// ----LCD----
LiquidCrystal_I2C lcd(0x3f, 20, 4); // I2C address 0x3f, 20 column and 4 rows

struct lcdStateData {
    float lastTargetTemperature;
    float lastReadTemperature;
} lcdState;

byte degreeChar[] = {
    B01000,
    B10100,
    B01000,
    B00011,
    B00100,
    B00100,
    B00100,
    B00011
};
// --END LCD--
// ----Keypad----
const byte ROWS = 4; //four rows
const byte COLS = 4; //four columns
char keys[ROWS][COLS] = {
  {'1','2','3', 'A'},
  {'4','5','6', 'B'},
  {'7','8','9', 'C'},
  {'*','0','#', 'D'}
};
byte rowPins[ROWS] = {26, 27, 28, 29}; //connect to the row pinouts of the keypad
byte colPins[COLS] = {22, 23, 24, 25}; //connect to the column pinouts of the keypad

Keypad keypad = Keypad(makeKeymap(keys), rowPins, colPins, ROWS, COLS); 
// --END Keypad--

// Modes
boolean sensorMode = false;
boolean editMode = false;

void initTimer1() {    
    pinMode(HEATER_PIN, OUTPUT);                        // Set heater pin to output
    noInterrupts();                                     // Disable all interrupts for timer setup

    TCCR1A  = 0;                                        // Set timer registers to 0
    TCCR1B  = 0;
    TCNT1   = 0;
    OCR1A   = 62500;                                    // Set compare match register for 0.25 hz increments
    TCCR1B |= (1 << WGM12) | (1 << CS12) | (1 << CS10); // Turn on CTC mode and set CS10 and CS12 bits for 1024 prescaler
    TIMSK1 |= (1 << OCIE1A);                            // Enable timer compare interrupt
    
    interrupts();                                       // Enable interrupts
}

void initSettings() {
    int target = EEPROM.read(0);
    if (target < TARGET_TEMP_MIN) {
        target = TARGET_TEMP_MIN;
    }
    if (target > TARGET_TEMP_MAX) {
        target = TARGET_TEMP_MAX;
    }
    
    targetTemperature = target;
}

void initEthernet() {
    pinMode(10, OUTPUT);        // set ethernet CS as output:
    pinMode(53, OUTPUT);        // set ATMega-2560, pin 53 hardware cs pin output, even if un-used:

    Serial.begin(9600);
    while (!Serial) {
        ; // wait for serial port to connect. Needed for native USB port only
    }

    // start the Ethernet connection:
    delay(500);

    Serial.println("Initialize Ethernet with DHCP:");

    if (Ethernet.begin(mac) == 0) {
        Serial.println("Failed to configure Ethernet using DHCP");
        if (Ethernet.hardwareStatus() == EthernetNoHardware) {
            Serial.println("Ethernet shield was not found.  Sorry, can't run without hardware. :(");
        } else if (Ethernet.linkStatus() == LinkOFF) {
            Serial.println("Ethernet cable is not connected.");
        }
    }

    // print your local IP address:
    Serial.print("My IP address: ");
    Serial.println(Ethernet.localIP());
}

void mqttReconnect() {
    while (!client.connected()) {
        Serial.println("Connecting to MQTT...");

        if (client.connect("ThermostatClient")) {
            Serial.println("connected");
        } else {
            Serial.print("failed with state ");
            Serial.print(client.state());
            delay(2000);
        }
    }
}

void initMqtt() {
    client.setServer(mqttServer, mqttPort);
    mqttReconnect();
}

void initLcd() {
    lcd.init();
    lcd.backlight();

    lcd.createChar(0, degreeChar);

    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Current: ");
    lcd.setCursor(0, 2);
    lcd.print("Target:  ");
}

void loopEthernet() {
    switch (Ethernet.maintain()) {
        case 1:
            //renewed fail
            Serial.println("Error: renewed fail");
            break;

        case 2:
            //renewed success
            Serial.println("Renewed success");
            //print your local IP address:
            Serial.print("My IP address: ");
            Serial.println(Ethernet.localIP());
            break;

        case 3:
            //rebind fail
            Serial.println("Error: rebind fail");
            break;

        case 4:
            //rebind success
            Serial.println("Rebind success");
            //print your local IP address:
            Serial.print("My IP address: ");
            Serial.println(Ethernet.localIP());
            break;

        default:
            //nothing happened
            break;
    }
}

void sendMqttMsg(const char* topic, float data) {
    char str_data[6];
    dtostrf(data, 4, 2, str_data);
    sprintf(dataString,"{\"value\":%s}", str_data);
    client.publish(topic, dataString, true);  // true means retained mqtt message
}

float getPotValue() {
    noInterrupts();
    ADMUX  = B01000111;
    ADCSRA = B11000111;
    while ((ADCSRA & B01000000) != 0);
    int adc = ADC;
    interrupts();
    float PotValue = ((float) adc / 1023) * 15;
    float x = 0.5 < (PotValue - (int) PotValue) ? 0.5 : 1;
    PotValue = (int) PotValue + 1 - x;
    return PotValue + 15;
}

char readKeypad() {
  char key = keypad.getKey();
   if (key) {
      Serial.print("Key ");
      Serial.print(key);
      Serial.println(" pressed");
  }
  return key;
}

void processKeypadInput(char input) {
  if(!input) return;
  switch (input) {
      // Override on
      case '*':
          if (sensorMode == true) return;
          Serial.println("Override ON");
          lcd.setCursor(0, 3);
          lcd.print("Override");
          sensorMode = true;
          // Send override on message to web app
          sendMqttMsg(overrideOnTopicName, 1);
          break;
      // Override off
      case '#':
          if (sensorMode == false) return;
          Serial.println("Override OFF");
          lcd.setCursor(0, 3);
          lcd.print("        ");
          sensorMode = false;
          // Send override off message to web app
          sendMqttMsg(overrideOnTopicName, 0);
          break;
      // Target edit on
      case 'A':
          if (editMode == true) return;
          Serial.println("Edit mode On");
          lcd.setCursor(16, 3);
          lcd.print("Edit");
          editMode = true;
          break;
      // Target edit off
      case 'B':
          if (editMode == false) return;
          Serial.println("Edit mode Off");
          lcd.setCursor(16, 3);
          lcd.print("    ");
          editMode = false;
          break;
      // Target + 1 celsius
      case '3':
          if (editMode == false) return;
          Serial.println("Target temperature + 1 celsius");
          if(targetTemperature == TARGET_TEMP_MAX) return;
          targetTemperature += 1;
          break;
      // Target - 1 celsius
      case '6':
          if (editMode == false) return;
          Serial.println("Target temperature - 1 celsius");
          if(targetTemperature == TARGET_TEMP_MIN) return;
          targetTemperature -= 1;
          break;
      // Save target to EEPROM
      case 'D':
          EEPROM.write(0, targetTemperature);
          lcd.setCursor(0, 1);
          lcd.print("Target saved!");
          delay(2000);
          lcd.setCursor(0, 1);
          lcd.print("             ");
          break;
  }
}

float readSensors(bool mode) { // false = using dallas sensor, true = using manual temperature override
  float temperature = 0.0;
  if (mode){
    float temperature = getPotValue();
    return temperature;
  }
    sensors.requestTemperatures();
  temperature = sensors.getTempCByIndex(0);
  if ((temperature - (int) temperature) < 0.75 && (temperature - (int) temperature) >= 0.25){
    return (int) temperature + 0.5;
  }
  if ((temperature - (int) temperature) >= 0.75) {
    return (int) temperature + 1.0;
  }
  if ((temperature - (int) temperature) < 0.25) {
    return (int) temperature + 0.0;
  }
  return temperature;
}

void processSensorData(float temp) {
    deltaTemperature = targetTemperature - temp;    // set delta temp
}

void updateLcd() {
    float currentTemp = targetTemperature - deltaTemperature;
    currentTemp = (float)((int)(currentTemp * 10 + 0.5f)) / 10.0f; // round to 1 decimal
    float targetTemp = (float)((int)(targetTemperature * 10 + 0.5f)) / 10.0f; // round to 1 decimal

    if (currentTemp == lcdState.lastReadTemperature && targetTemp == lcdState.lastTargetTemperature) {
        // Don't update lcd if nothing needs updating
        return;
    }

    // Send current temperature to web app
    sendMqttMsg(currentTempTopicName, currentTemp);
    // Send current target to web app
    sendMqttMsg(targetTempTopicName, targetTemp);

    lcd.setCursor(9, 0);        // Set to first row
    lcd.print(currentTemp);     // Print current temp reading
    lcd.print(' ');             // Space before degree char
    lcd.write(0);               // Degree char as custom char
    lcd.print("   ");           // Spaces to clear out possibly remaining text from previous loops
    lcd.setCursor(9, 2);        // Set to third row
    lcd.print(targetTemp);      // Print current target reading
    lcd.print(' ');             // Repeat space/degree/space writes
    lcd.write(0);               // re
    lcd.print("   ");           // re

    // Save last written readings
    lcdState.lastReadTemperature = currentTemp;
    lcdState.lastTargetTemperature = targetTemp;
}

void setup() {
   initTimer1();
   initSettings();
   initEthernet();
   initMqtt();
   initLcd();
   sensors.begin();
    // Enable watchdog timer on 8s timeout
    wdt_enable(WDTO_8S);
}

void loop() {
    // Reset watchdog timer
    wdt_reset();

    loopEthernet();
    // mqtt reconnect
    if (!client.connected()) {
        mqttReconnect();
    }

    wdt_reset();

    char kpInput = readKeypad();
    processKeypadInput(kpInput);

    wdt_reset();

    float temp = readSensors(sensorMode);
    processSensorData(temp);

    wdt_reset();

    updateLcd();
}

ISR(TIMER1_COMPA_vect) {
    // ISR will be called on 4 second intervals
    if (deltaTemperature > 0) {
        // heater on if we are under target temperature
        digitalWrite(HEATER_PIN, HIGH);
        // Send heater on message to web app
        sendMqttMsg(heaterOnTopicName, 1);
        heaterStatus = true;
    } else {
        digitalWrite(HEATER_PIN, LOW);
        // Send heater off message to web app
        sendMqttMsg(heaterOnTopicName, 0);
        heaterStatus = false;
    }
}

ISR(WDT_vect) {
    // no-op
}