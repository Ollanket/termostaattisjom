# update local with remote changes

- // if you already have local changes, commit them (git commit -m "<message>")
- git pull

- // if merge conflicts, check file and manually fix, then
- git add <conflicted file>
- git commit
- // exit editor out to complete commit

- git push



# push changes to remote branch

- // before committing changes

- git checkout -b <branch name>

- // commit changes

- // if pushing changes for first time on branch
- git push -u origin <branch name>

- // else
- git push

